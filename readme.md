# sysinfgo

Get useful software and hardware info about a Linux system in one contained log, written in go.  

Currently finds:  
* core count, thread count, name and vendor for every installed CPU
* name and vendor for every installed GPU
* physical and available memory
* kernel version
* installed java runtime, if one is present
* all hosts file entries
* ID and name for every running process  

Available command line flags:  
* -nolog  
disable logging to file
* -nolive  
disable live console output (does not silence errors)

Planned changes:
* ~~implement a proper logger~~  
* log graphics driver info
* add a hosts editor
* ~~make main.go less ew~~   
* ~~add some command line options, such as one to disable console output and focus the log~~  

**Prebuilt binares can be found in the bin directory, but these will not always be up to date with new features found in the source.**  
**MacOS support is not yet tested, you may have to remove some features when building.**  


so what 90% of the code is in the imports?
