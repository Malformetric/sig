package main

import (
	"fmt"
	"time"
	"os"
	"flag"
	"runtime"
)

var Logger logger

func main() {
	noLog := flag.Bool("nolog", false, "disable output to log")
	noLive := flag.Bool("nolive", false, "disable live output to console")
	flag.Parse()

	if runtime.GOOS != "linux" && runtime.GOOS != "darwin" {
		Logger.error("this code should not be compiled for your platform")
		os.Exit(3)
	}

	Logger, err := newLogger("./sig-log.txt", !*noLog, !*noLive)
	evaluate(err)
	defer Logger.close()
	startTime := time.Now()

	cpuInfoItem, err := cpuInfo()
	evaluate(err)
	gpuInfoItem, err := gpuInfo()
	evaluate(err)
	memInfoItem, err := memInfo()
	evaluate(err)

	kernelInfoItem, err := kernelInfo()
	evaluate(err)
	javaInfoItem, err := javaInfo()
	evaluate(err)
	hostInfoItem, err := hostInfo()
	evaluate(err)
	processInfoItem, err := processInfo()
	evaluate(err)

	infoItems := []infoItem{
		cpuInfoItem,
		gpuInfoItem,
		memInfoItem,
		kernelInfoItem,
		javaInfoItem,
		hostInfoItem,
		processInfoItem,
	}

	for _, item := range(infoItems) {
		for _, line := range(item.lines) {
			if item.tag == "" {
				Logger.raw(line)
			} else {
				Logger.custom(item.tag, line)
			}
		}

		Logger.raw("") // spacer
	}

	elapsedTime := time.Since(startTime)
	Logger.raw(fmt.Sprintf("log completed in %s", elapsedTime))
}

type infoItem struct {
	tag   string
	lines []string
}

func evaluate(e error) {
	if e != nil {
		Logger.error(e.Error())
		os.Exit(1)
	}
}