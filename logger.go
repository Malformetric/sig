package main

import (
	"os"
	"fmt"
)

type logger struct {
	file      *os.File
	toLog    bool
	toConsole bool
}

func newLogger(p string, l bool, c bool) (logger, error) {
	file, err := os.Create(p)
	if err != nil { return logger{}, err }
	return logger{file, l, c}, nil
}

func (l *logger) toFile(s string) {
	l.file.WriteString(s + "\n")
}

func (l *logger) raw(s string) {
	if l.toLog { l.toFile(s) }
	if l.toConsole { fmt.Println(s) }
}

func (l *logger) custom(t string, s string) {
	str := fmt.Sprintf("[%v] %v", t, s)
	l.raw(str)
}

func (l *logger) error(s string) {
	str := "[ERROR] " + s
	l.toFile(str)
	fmt.Println(str)
}

func (l *logger) close() {
	l.file.Close()
}

// TODO add logger.blank for a variable number of blank lines